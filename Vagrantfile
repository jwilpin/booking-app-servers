# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

$install_mysql = <<SCRIPT
    set -x
    echo "I am provisioning mysql..."
    date >> /etc/vagrant_provisioned_at
    echo "install mysql.."
    yum install mariadb-server -y
    /sbin/service mariadb start
    mysql -e "create user 'booking'@'%' identified by 'password';"
    mysql -e "create database booking character set UTF8 collate utf8_bin;"
    mysql -e "GRANT ALL PRIVILEGES ON booking.* TO 'booking'@'%';"
    mysql -e "flush privileges;"
SCRIPT

$install_java8 = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am provisioning java..."
    yum install java-devel -y
SCRIPT

$install_sbt = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am provisioning SBT..."
    curl https://bintray.com/sbt/rpm/rpm | sudo tee /etc/yum.repos.d/bintray-sbt-rpm.repo
    yum install sbt -y
SCRIPT

$install_git = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am provisioning GIT..."
    yum install git -y
SCRIPT


$install_nodejs = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am provisioning NODEJS..."
    yum install epel-release -y
    yum install nodejs -y
    yum install npm -y
    npm config set proxy $http_proxy
    npm config set https-proxy $http_proxy
    npm config set no-proxy $no_proxy
    npm config set proxy true
SCRIPT

$install_npm_grunt = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am provisioning NPM GRUNT..."
     npm install -g grunt-cli
SCRIPT

$install_npm_yoman = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am provisioning NPM YOMAN..."
    npm install -g yo
SCRIPT

$install_npm_bower = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am provisioning NPM BOWER..."
    npm install -g bower
SCRIPT

$git_booking_app_frontend = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am cloning frontend repo..."
    rm -Rf /opt/frontend/
    git clone https://jwilpin@bitbucket.org/jwilpin/booking-app-frontend.git /opt/frontend/
    yes | cp -Rf /vagrant/app.js /opt/frontend/app/scripts/app.js
SCRIPT

$git_booking_app_backend = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am cloning backend repo..."
    rm -Rf /opt/backend/
    git clone https://jwilpin@bitbucket.org/jwilpin/booking-app-backend.git /opt/backend/
    yes | cp -Rf /vagrant/application.conf /opt/backend/conf/application.conf
    yes | cp -Rf /vagrant/build.sbt /opt/backend/build.sbt
SCRIPT

$run_booking_app_backend = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am running backend..."
    cd /opt/backend/
    sbt compile
    sbt run
SCRIPT

$run_booking_app_frontend = <<SCRIPT
    set -x
    date >> /etc/vagrant_provisioned_at
    echo "I am running frontend..."
    git clone https://github.com/Medium/phantomjs /opt/phantomjs/
    cd /opt/phantomjs/
    npm install
    node ./install.js -g
    cd /opt/frontend/
    npm install
    bower install --allow-root
    grunt build --force
    grunt serve
SCRIPT


Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  
  if Vagrant.has_plugin?("vagrant-proxyconf")
    config.proxy.http     = "http://proxy.corp.globant.com:3128"
    config.proxy.https    = "https://proxy.corp.globant.com:3128"
    config.proxy.no_proxy = "localhost,127.0.0.1,github.com,frontendserver,backendserver,dbserver,192.168.33.10,192.168.33.20,192.168.33.30"
  end


  config.vm.box = "centos/7"

  config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/", rsync__auto: "true"

  

  config.vm.define :db_server do |v|
    v.vm.network "private_network", ip: "192.168.33.30"
    v.vm.host_name = "dbserver"
    v.vm.network :forwarded_port, guest: 22, host: 3322
    v.vm.provider "virtualbox" do |vb|
            vb.cpus = 1
            vb.memory = "512"
    end
    v.vm.provision :shell, :inline => $install_mysql
  end

  config.vm.define :backend_server do |v|
    v.vm.network "private_network", ip: "192.168.33.20"
    v.vm.host_name = "backendserver"
    config.vm.network :forwarded_port, guest: 22, host: 3323
    v.vm.provider "virtualbox" do |vb|
            vb.cpus = 1
            vb.memory = "2048"
    end
    v.vm.provision :shell, :inline => 'sudo sh -c "echo 192.168.33.30 dbserver >> /etc/hosts"'
    v.vm.provision :shell, :inline => $install_java8
    v.vm.provision :shell, :inline => $install_sbt
    v.vm.provision :shell, :inline => $install_git
    v.vm.provision :shell, :inline => $git_booking_app_backend
    v.vm.provision :shell, :inline => $run_booking_app_backend
  end
  
  config.vm.define :frontend_server do |v|
    v.vm.network "private_network", ip: "192.168.33.10"
    v.vm.host_name = "frontendserver"
    config.vm.network :forwarded_port, guest: 22, host: 3324
    v.vm.provider "virtualbox" do |vb|
            vb.cpus = 1
            vb.memory = "512"
    end
    v.vm.provision :shell, :inline => 'sudo sh -c "echo 192.168.33.20 backend >> /etc/hosts"'
    v.vm.provision :shell, :inline => $install_nodejs
    v.vm.provision :shell, :inline => $install_npm_grunt
    v.vm.provision :shell, :inline => $install_npm_yoman
    v.vm.provision :shell, :inline => $install_npm_bower
    v.vm.provision :shell, :inline => $install_git
    v.vm.provision :shell, :inline => $git_booking_app_frontend
    v.vm.provision :shell, :inline => $run_booking_app_frontend
  end



  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
